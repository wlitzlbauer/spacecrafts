
class 'LuaController' (AIController)

function LuaController:__init(spacecraft, human, path)
    super(spacecraft, human, path)
end

function LuaController:update(delta)
    log("Update")
end

controllers = {}

function createLuaController(name, spacecraft, human, path)
    controllers[name] = LuaController(spacecraft, human, path)
    return controllers[name]
end
