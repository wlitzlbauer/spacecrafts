sampler2D RT : register(s0);
uniform float invertFactor;

float4 Invert_ps (	float4 pos : POSITION, float2 iTexCoord : TEXCOORD0) : COLOR
{
	return invertFactor - tex2D(RT, iTexCoord);
}
