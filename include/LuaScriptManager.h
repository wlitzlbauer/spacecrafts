/**
* LuaScriptManager.h
*/

#pragma once

#include <luabind/luabind.hpp>

namespace scripting { class Manager; }

//template class _OgreExport Ogre::Singleton<scripting::Manager>; // export of explicit instantiation

class LuaScriptManager : public Ogre::Singleton<LuaScriptManager>
{
public:

	LuaScriptManager();
	~LuaScriptManager();
	LuaScriptManager(const LuaScriptManager &other);
	LuaScriptManager& operator= (const LuaScriptManager &other);

	void runScriptFile(const std::string& file);
	void runScript(const std::string& code);

	bool hasGlobalFunction(const std::string& funcName);
	luabind::object getGlobalTable() const;
	const lua_State* getInterpreter() const;

	template <class RET>
	RET callFunction(const char* funcName)
	{
		return luabind::call_function<RET>(mMasterState, funcName);
	}

	template <class RET, class ARG1>
	RET callFunction(const char* funcName, ARG1 arg1)
	{
		return luabind::call_function<RET>(mMasterState, funcName, arg1);
	}

	template <class RET, class ARG1, class ARG2>
	RET callFunction(const char* funcName, ARG1 arg1, ARG2 arg2)
	{
		return luabind::call_function<RET>(mMasterState, funcName, arg1, arg2);
	}

	template <class RET, class ARG1, class ARG2, class ARG3>
	RET callFunction(const char* funcName, ARG1 arg1, ARG2 arg2, ARG3 arg3)
	{
		return luabind::call_function<RET>(mMasterState, funcName, arg1, arg2, arg3);
	}

	static LuaScriptManager& getSingleton(void);
	static LuaScriptManager* getSingletonPtr(void);

	void setScriptPath(const Ogre::String& path);

	void update();

private:
	lua_State* mMasterState;

	static int panicHandler(lua_State* L);
	static int loadLuaLibs(lua_State *L);
	static const char* chunkReader(lua_State *L, void *data, size_t *size);
};