 #ifndef __AIController_h_
 #define __AIController_h_ 

#include "SpacecraftController.h"
#include "Path.h"
#include "luabind/object.hpp"


class AIController: public SpacecraftController
{
public:
	AIController(Spacecraft* spacecraft, Spacecraft* mHumanSpacecraft, const Path& patrol);

	virtual void update(float delta);

	// returns the patrol path of this player
	const Path& getPatrolPath() const
	{
		return mPath;
	}

	// returns the human craft
	Spacecraft* getHumanSpacecraft()
	{
		return mHumanSpacecraft;
	}

	/// returns the start position of this ai player
	Ogre::Vector3 getStartPosition()
	{
		return mPath.getPosition(0);
	}

private:
	Spacecraft* mHumanSpacecraft;

	Path mPath;
	float mCurrentParam;

	luabind::object root;
};


#endif