 #ifndef __SpacecraftController_h_
 #define __SpacecraftController_h_ 

#include "Spacecraft.h"

class Path;

class SpacecraftController
{
public:
	SpacecraftController(Spacecraft* spacecraft);
	virtual ~SpacecraftController();

	virtual void update(float delta) = NULL;

	Spacecraft* getSpacecraft()
	{
		return mSpacecraft;
	}

public:
	Ogre::Vector3 seek(const Ogre::Vector3& target);
	Ogre::Vector3 arrive(const Ogre::Vector3& target);
	Ogre::Vector3 pursue(Spacecraft* spacecraft);
	Ogre::Vector3 obstacleAvoidance();
	Ogre::Vector3 separation();
	Ogre::Vector3 wallAvoidance();
	Ogre::Vector3 wander();
	Ogre::Vector3 followPath(const Path& path);

	Spacecraft* mSpacecraft;

	float mWanderOrientation;
};

#endif