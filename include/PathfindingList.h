#ifndef __PATHFINDING_LIST_H__
#define __PATHFINDING_LIST_H__

#include <vector>
#include "Connection.h"

class NavigationNode;
class Connection;

/// A list of Nodes with meta information used for pathfinding.
/// Note: This implementation uses a vector to store the NodeRecord.
///       This is far from optimal and should be replaced by a priority queue or heap.
class PathfindingList
{
	friend class NavigationGraph;
public:
	struct NodeRecord
	{
		NavigationNode* node;
		Connection connection;
		float costSoFar;
		float estimatedTotalCost;

		NodeRecord(){}

		NodeRecord(NavigationNode* node, Connection connection, float costSoFar, float estimatedTotalCost = 0.0f) :
			node(node),
			connection(connection),
			costSoFar(costSoFar),
			estimatedTotalCost(estimatedTotalCost)
		{}
	};

	static NodeRecord EMPTY_RECORD;
	
	/// adds a new Record to the list.
	void add(const NodeRecord& record)
	{
		mRecords.push_back(record);
	}
	
	/// Removes the NodeRecord related to the given Node.
	void remove(NavigationNode* node);
	
	/// Returns NodeRecord with the smallest estimated total costs.
	NodeRecord& getSmallest();
	
	/// Returns NodeRecord with the smallest costs (use only for Dijkstra).
	NodeRecord& getSmallestPerCostSoFar();
	
	/// Finds the NodeRecord related to the given Node.
	NodeRecord& find(NavigationNode* node);
	
	/// Checks if there is NodeRecord for the given Node.
	bool contains(NavigationNode* node);
	
	bool isEmpty()
	{
		return mRecords.size() == 0;
	}
	
private:
	std::vector<NodeRecord> mRecords;
};

#endif

