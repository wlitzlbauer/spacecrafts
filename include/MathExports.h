#pragma once

// Library Include
extern "C"
{
	#include <lua.h> 
}

class MathExports
{
public:
	static void extendState(lua_State* pState);
};
