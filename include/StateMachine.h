#ifndef __StateMachine_h_
#define __StateMachine_h_ 

#include "luabind/luabind.hpp"

class AIController;
class Transition;

class State
{
public:
	State(AIController* controller) :
		mController(controller)
	{}

	virtual ~State();

	virtual void update(float delta) {}
	virtual void enter() {}
	virtual void exit() {}

	const std::vector<Transition*>& getTransitions() const
	{
		return mTransitions;
	}

	void addTransition(Transition* transition)
	{
		mTransitions.push_back(transition);
	}

protected:
	AIController* mController;

private:
	std::vector<Transition*> mTransitions;
};

class Transition
{
public:
	Transition(AIController* controller, State* targetState) :
		mTargetState(targetState),
		mController(controller)
	{}

	virtual ~Transition() {}

	virtual bool isTriggered() = 0;

	State* getTargetState()
	{
		return mTargetState;
	}

protected:
	AIController* mController;

private:
	State* mTargetState;
};


class StateMachine
{
public:
	StateMachine() :
		mCurrentState(NULL)
	{}

	virtual ~StateMachine();

	void setCurrentState(State* currentState)
	{
		mCurrentState = currentState;
	}

	void addState(State* state)
	{
		mStates.push_back(state);
	}

	void update(float delta);

	void switchState(State* targetState);

	State* getCurrentState()
	{
		return mCurrentState;
	}

private:
	std::vector<State*> mStates;
	State* mCurrentState;
};


#endif