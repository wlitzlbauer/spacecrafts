 #ifndef __Game_Application_h_
 #define __Game_Application_h_ 

#include "BaseApplication.h"

using namespace Ogre;
using namespace OgreBulletDynamics;
using namespace OgreBulletCollisions;

class LuaScriptManager;
class Spacecraft;
class SpacecraftController;
class HumanController;
class Rocket;
class NavigationGraph;
class GameConfig;
class DebugOverlay;
class ICollider;

class GameApplication : public BaseApplication, Ogre::Singleton<GameApplication>
{
public:
 	GameApplication();
 
 	~GameApplication();

	static GameApplication& getSingleton(void);

    static GameApplication* getSingletonPtr(void);

	/// creates a new rocket at the given position.
	void createRocket(const Vector3& position, const Vector3& direction, ICollider* emitter);

	/// releases a rocket (-> delete the rocket in the next frame)
	void releaseRocket(Rocket* rocket);

	std::vector<Spacecraft*>& getSpacecrafts()
	{
		return mSpacecrafts;
	}

	OgreBulletDynamics::DynamicsWorld* getPhysics()
	{
		return mWorld;
	}
 
 protected:
    virtual bool configure(void);

	virtual void createScene(void);

	virtual void createSpacecrafts(void);

	virtual void createCamera(void);

	virtual void createWalls(void);

	void createDynamicWorld(Vector3 &gravityVector, AxisAlignedBox &bounds);

	virtual bool frameStarted(const Ogre::FrameEvent& evt);
	virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);
	virtual bool frameEnded(const Ogre::FrameEvent& evt);

	virtual bool keyPressed(const OIS::KeyEvent &arg);
    virtual bool keyReleased(const OIS::KeyEvent &arg);

private:
	enum CameraMode
	{
		CAMERA_FREE,
		CAMERA_FOLLOW_PLAYER,
		CAMERA_TOP,
		CAMERA_COUNT
	};

	/// OgreBullet World
 	OgreBulletDynamics::DynamicsWorld* mWorld;	

	// DebugDrawer for the physics world
 	OgreBulletCollisions::DebugDrawer* mDebugDrawer;	

	// Util for text debug rendering.
	DebugOverlay* mDebugOverlay;

	GameConfig* mGameConfig;

	std::vector<Spacecraft*> mSpacecrafts;
	std::vector<SpacecraftController*> mControllers;
	std::list<Rocket*> mRockets;
	std::list<Rocket*> mReleasedRockets;

	HumanController* mHumanController;
	int mRocketCounter;

	LuaScriptManager* mScriptingManager;

	bool mShowDebugDraw;
	bool mShowNavigationGraph;
	CameraMode mCameraMode;
	bool mPause;
	bool mStep;

	void update(float delta);
 };

#endif
