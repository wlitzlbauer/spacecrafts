#pragma once

// Library Include
extern "C"
{
	#include <lua.h> 
}


class GameExports
{
public:
	static void extendState(lua_State* pState);
};
