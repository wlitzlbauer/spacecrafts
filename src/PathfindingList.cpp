#include "stdafx.h"

#include "PathfindingList.h"

PathfindingList::NodeRecord PathfindingList::EMPTY_RECORD;


void PathfindingList::remove(NavigationNode* node)
{
	std::vector<NodeRecord>::iterator it;
	for (it = mRecords.begin(); it != mRecords.end(); it++)
	{
		if ((*it).node == node)
		{
			mRecords.erase(it);
			return;
		}
	}
}

PathfindingList::NodeRecord& PathfindingList::getSmallest()
{
	if (isEmpty())
	{
		return EMPTY_RECORD;
	}
	
	NodeRecord& s = mRecords[0];
	
	for (int i = 1; i < (int)mRecords.size(); i++)
	{
		if (mRecords[i].estimatedTotalCost < s.estimatedTotalCost)
		{
			s = mRecords[i];
		}
	}
	
	return s;
}

PathfindingList::NodeRecord& PathfindingList::getSmallestPerCostSoFar()
{
	if (isEmpty())
	{
		return EMPTY_RECORD;
	}
	
	NodeRecord& s = mRecords[0];
	
	for (int i = 1; i < (int)mRecords.size(); i++)
	{
		if (mRecords[i].costSoFar < s.costSoFar)
		{
			s = mRecords[i];
		}
	}
	
	return s;
}

PathfindingList::NodeRecord& PathfindingList::find(NavigationNode* node)
{
	for (int i = 0; i < (int)mRecords.size(); i++)
	{
		if (mRecords[i].node == node)
		{
			return mRecords[i];
		}
	}
	
	return EMPTY_RECORD;
}

bool PathfindingList::contains(NavigationNode* node)
{
	for (int i = 0; i < (int)mRecords.size(); i++)
	{
		if (mRecords[i].node == node)
		{
			return true;
		}
	}
	
	return false;
}