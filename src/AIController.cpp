#include "StdAfx.h"

#include <algorithm> 
#include "AIController.h"
#include "DebugOverlay.h"

#include "DebugDisplay.h"

#include "NavigationGraph.h"
#include "GameConfig.h"
#include "GameApplication.h"
#include "MathUtil.h"
#include "LuaBind/object.hpp"
#include "LuaScriptManager.h"

using namespace Ogre;


AIController::AIController(Spacecraft* spacecraft, Spacecraft* humanSpacecraft, const Path& patrol):
	SpacecraftController(spacecraft),
	mHumanSpacecraft(humanSpacecraft),
	mPath(patrol)
{
}


void AIController::update(float delta)
{
	Vector3 wander = this->wander();
	
	mSpacecraft->setSteeringCommand(wander);
}


 
