#include "stdafx.h"
#include "LuaScriptManager.h"
#include <strstream>
#include <iostream>
#include <fstream>

extern "C"
{
	#include <lauxlib.h> 
	#include <lualib.h>
}

#include <luabind/object.hpp>
#include <luabind/class_info.hpp>

#ifdef WIN32
	#define WIN32_LEAN_AND_MEAN
	#define NOMINMAX
	#include <Windows.h>
#endif

#include "MathExports.h"
#include "OgreExports.h"
#include "GameExports.h"

// Template specialization
template<> LuaScriptManager* Ogre::Singleton<LuaScriptManager>::msSingleton = 0;


LuaScriptManager* LuaScriptManager::getSingletonPtr(void)
{
	return msSingleton;
}
LuaScriptManager& LuaScriptManager::getSingleton(void)
{
	assert(msSingleton);  return (*msSingleton);
}


//-----------------------------------------------------------------------
LuaScriptManager::LuaScriptManager() : mMasterState(NULL)
{
	luabind::disable_super_deprecation();

	mMasterState = lua_open(); 
	luaL_openlibs(mMasterState);

	lua_cpcall(mMasterState, &LuaScriptManager::loadLuaLibs, NULL);
	lua_atpanic(mMasterState, &LuaScriptManager::panicHandler);

	luabind::open(mMasterState);

	lua_gc(mMasterState, LUA_GCSTOP, 0);
	MathExports::extendState(mMasterState);
	OgreExports::extendState(mMasterState);
	GameExports::extendState(mMasterState);
	lua_gc(mMasterState, LUA_GCRESTART, 0);

	luabind::bind_class_info(mMasterState);

	lua_pushstring(mMasterState, "Client VM");
	lua_setglobal(mMasterState, "decoda_name");
}
//-----------------------------------------------------------------------	
LuaScriptManager::~LuaScriptManager()
{
	lua_close(mMasterState);
}
//-----------------------------------------------------------------------
LuaScriptManager::LuaScriptManager(const LuaScriptManager &other)
{
	*this = other;
}
//-----------------------------------------------------------------------
LuaScriptManager& LuaScriptManager::operator= (const LuaScriptManager &other)
{
	if (this == &other)
		return *this;

	mMasterState = other.mMasterState;
	return *this;
}
//-----------------------------------------------------------------------
int LuaScriptManager::panicHandler(lua_State* L)
{
	std::string str = lua_tostring(L, 1);
#ifdef WIN32
	MessageBox(NULL, str.c_str(), "Scripting error...", MB_ICONERROR | MB_OKCANCEL);
#else
	std::cout << "Scripting error: " << str << std::endl;
#endif
	return 0;
}
//-----------------------------------------------------------------------
int LuaScriptManager::loadLuaLibs(lua_State *L)
{
	lua_gc(L, LUA_GCSTOP, 0);  // stop collector during initialization 
	luaL_openlibs(L);  // open libraries 
	luaopen_debug(L);
	lua_gc(L, LUA_GCRESTART, 0);
	return 0;
}

void LuaScriptManager::update()
{}

void LuaScriptManager::setScriptPath(const Ogre::String& path)
{
	lua_getglobal(mMasterState, "package");
	lua_getfield(mMasterState, -1, "path"); // get field "path" from table at top of stack (-1)
	std::string cur_path = lua_tostring(mMasterState, -1); // grab path string from top of stack
	std::string new_path = path;
	new_path.append("/?.lua;"); // do your path magic here
	new_path.append(cur_path);
	lua_pop(mMasterState, 1); // get rid of the string on the stack we just pushed on line 5
	lua_pushstring(mMasterState, new_path.c_str()); // push the new one
	lua_setfield(mMasterState, -2, "path"); // set the field "path" in table at -2 with value at top of stack
	lua_pop(mMasterState, 1); // get rid of package table from top of stack
}

//-----------------------------------------------------------------------
void LuaScriptManager::runScriptFile(const std::string& file)
{
	luaL_dofile(mMasterState, file.c_str());
}
//-----------------------------------------------------------------------
void LuaScriptManager::runScript(const std::string& code)
{
	if (code.length() <= 0)
		return;

	std::strstream luaCodeReader;
	luaCodeReader << code;

	if (lua_load(mMasterState, &LuaScriptManager::chunkReader, &luaCodeReader, "LuaScriptManager") == 0)
	{
		if (lua_pcall(mMasterState, 0, 0, 0) != 0)
		{
			std::string errStr = lua_tostring(mMasterState, -1);
#ifdef WIN32
			MessageBox(NULL, errStr.c_str(), "Scripting error...", MB_ICONERROR | MB_OKCANCEL);
#else
			std::cout << "Scripting error: " << errStr << std::endl;
#endif
		}
	}
	else
	{
		// the top of the stack should be the error string
		if (!lua_isstring(mMasterState, lua_gettop(mMasterState)))
			return;

		// get the top of the stack as the error and pop it off
		std::string errStr = lua_tostring(mMasterState, lua_gettop(mMasterState));
		lua_pop(mMasterState, 1);

#ifdef WIN32
		MessageBox(NULL, errStr.c_str(), "Scripting error...", MB_ICONERROR | MB_OKCANCEL);
#else
		std::cout << "Scripting error: " << errStr << std::endl;
#endif
	}
}
//-----------------------------------------------------------------------
const char* LuaScriptManager::chunkReader(lua_State *L, void *data, size_t *size)
{
	const size_t kBufferSize = 1024;
	static char s_buffer[kBufferSize];

	std::istream* pis = reinterpret_cast<std::istream*>(data);
	if ( !pis )
		return NULL;
	pis->read( &s_buffer[0], kBufferSize );
	*size = (size_t)pis->gcount();
	return s_buffer;
}
//-----------------------------------------------------------------------
luabind::object LuaScriptManager::getGlobalTable() const
{
	return luabind::globals(mMasterState);
}
//-----------------------------------------------------------------------
bool LuaScriptManager::hasGlobalFunction(const std::string& funcName)
{
	lua_getfield(mMasterState, LUA_GLOBALSINDEX, funcName.c_str());
	bool bFound = !lua_isnil(mMasterState, -1);
	lua_pop(mMasterState,1);
	return bFound;
}
//-----------------------------------------------------------------------
const lua_State* LuaScriptManager::getInterpreter() const
{
	return mMasterState;
}
//-----------------------------------------------------------------------