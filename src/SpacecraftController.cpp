#include "StdAfx.h"

#include "SpacecraftController.h"
#include "DebugDisplay.h"
#include "GameConfig.h"
#include "Path.h"
#include "MathUtil.h"
#include "WorldUtil.h"

using namespace Ogre;


SpacecraftController::SpacecraftController(Spacecraft* spacecraft):
	mSpacecraft(spacecraft),
	mWanderOrientation(0.0f)
{
	mWanderOrientation = Math::PI;
}


SpacecraftController::~SpacecraftController()
{}

Ogre::Vector3 SpacecraftController::seek(const Ogre::Vector3& target)
{
	Ogre::Vector3 desiredVelocity = target - mSpacecraft->getPosition();
	desiredVelocity.normalise();
	desiredVelocity *= Spacecraft::MAX_SPEED;

	return desiredVelocity - mSpacecraft->getLinearVelocity();
}

Ogre::Vector3 SpacecraftController::arrive(const Ogre::Vector3& target)
{
	DebugDisplay::getSingleton().drawCircle(target, 1.0f, 16, ColourValue::Red);

	Ogre::Vector3 toTarget = target - mSpacecraft->getPosition();
	float dist = toTarget.length();

	if (dist > 0)
	{
		const float DECELERATION_TWEAKER = GameConfig::getSingleton().getValueAsReal("Steering/DecelerationTweaker");
		float speed = dist / DECELERATION_TWEAKER;

		speed = std::min(speed, Spacecraft::MAX_SPEED);
		Ogre::Vector3 desiredVelocity = toTarget * speed / dist;

		return desiredVelocity - mSpacecraft->getLinearVelocity();
	}

	return Ogre::Vector3(0.0f);
}

Ogre::Vector3 SpacecraftController::pursue(Spacecraft* spacecraft)
{
	return seek(spacecraft->getPosition());
}

Ogre::Vector3 SpacecraftController::followPath(const Path& path)
{
	return Ogre::Vector3(0.0f);
}

Ogre::Vector3 SpacecraftController::wallAvoidance()
{
	return Vector3(0.0f);
}


Ogre::Vector3 SpacecraftController::obstacleAvoidance()
{
	return Vector3(0.0f);
}

Ogre::Vector3 SpacecraftController::separation()
{
	Vector3 steering(0, 0, 0);
	Spacecraft* owner = getSpacecraft();
	const float LOOK_AHEAD_TIME = 1.0f;
	const float THRESHOLD = 30.0f;
	const float K = 1500.0f;


	// Loop through all targets
	for (Spacecraft* target : WorldUtil::getAllSpacecrafts())
	{
		if ((target == owner) || target->getFaction() != owner->getFaction())
		{
			// ignore enemy
			continue;
		}

		Vector3 distance = owner->getPosition() - target->getPosition();
		float distanceLen = distance.length();

		if (distanceLen < THRESHOLD) 
		{
			float strength = std::min(K / (distanceLen * distanceLen), Spacecraft::MAX_SPEED);

			Vector3 direction = distance.normalisedCopy();

			// sum up all forces
			steering += strength * direction;
		}
	}

	DebugDisplay::getSingleton().drawLine(owner->getPosition(), owner->getPosition() + steering, ColourValue::Blue);
	return steering;
}


Vector3 SpacecraftController::wander()
{
	const float WANDER_DISTANCE = 40.0f;
	const float WANDER_RADIUS = 10.0f;
	const float WANDER_RATE = 0.2f;

	DebugDisplay::getSingleton().drawCircle(
		mSpacecraft->getPosition() +
		mSpacecraft->getDirection() * WANDER_DISTANCE,
		WANDER_RADIUS, 18, ColourValue::Green);

	mWanderOrientation += MathUtil::randomBinomial() * WANDER_RATE;

	Vector3 targetVec = Quaternion((Radian) mWanderOrientation, Vector3::UNIT_Y) * mSpacecraft->getDirection();

	Vector3 target = mSpacecraft->getPosition() + mSpacecraft->getDirection() * WANDER_DISTANCE;

	target += targetVec * WANDER_RADIUS;

	DebugDisplay::getSingleton().drawCircle(target, 1.0f, 18, ColourValue::Green);

	return seek(target);
}